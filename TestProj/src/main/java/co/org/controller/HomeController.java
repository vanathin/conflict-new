package co.org.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import co.org.dao.Student;
import co.org.service.StudentService;

@RequestMapping("/homeController")
@Controller
public class HomeController {

	@Autowired
	private StudentService stuService;
	
	@Autowired
	private Animal animal;
	
	
	@Value("${dao.id: test}") 
	private String jdbcUrl;
	  
	
	@RequestMapping(value ="/insert", method=RequestMethod.POST, headers="Accept=application/json")
	@ResponseBody
	public Student homePage(@RequestBody @Validated Student stu, BindingResult e){
	
	if (e.hasErrors()) {
		System.out.println("Binding Error");
        throw new InvalidRequestException("Input is wrong", e);
    }
		try {
			stuService.dummy1();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		try {
			stuService.dummy(5);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		System.out.println("###############3");
		return stu;
	}
	
	
	@RequestMapping(value="/viewPage")
	public String homePage(){
		System.out.println("in view page");
		return "forward:/testController/test";
		//return "redirect:/testController/test";
	}
	
	@RequestMapping(value="/viewPage1")
	public String homePage1(Model m){
		//m.addAttribute("name", "Test");
		return "redirect:/testController/test";
		//System.out.println("in view page1");
		//return "forward:/testController/test";
	}
	
	@RequestMapping(value="/savePage")
	public Student saveStudent(Student students){
		//service call which in turn calls dao to save the student
		return students;
	}
	
	//Obj will be created for animal and lion when application get starts up.
	@RequestMapping(value="/name")
	@ResponseBody
	public Animal autowireByName(){
		if(animal instanceof Animal){
			//System.out.println("TRUE1"+ animal.getLion1().getColor());
			System.out.println("TRUE2"+ animal.getS());
		}
		System.out.println("@@@");
		return animal;
	}
}
